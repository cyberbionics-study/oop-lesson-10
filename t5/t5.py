import re


def get_dict_from_input():
    res = {}
    input_data = input('Please enter your full name, birthdate, email, and a comment about your course: ')

    name_match = re.search(r'([A-Z][a-z]+( [A-Z][a-z]+){1,2}),', input_data)
    if name_match:
        res['full name'] = name_match.group(0)

    date_match = re.search(r'(\d{1,2}\.\d{1,2}\.\d{4}),', input_data)
    if date_match:
        res['birthdate'] = date_match.group(0)

    email_match = re.search(r'[-\w\\.]+@([\w-]+\.)+[\w-]{2,4}', input_data)
    if email_match:
        res['email'] = email_match.group(0)

    return res


if __name__ == '__main__':
    print(get_dict_from_input())

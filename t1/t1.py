import re
from collections import Counter


def count_word_frequency(text):
    words = re.findall(r'\w+', text.lower())
    word_count = Counter(words)
    return word_count


test_text = "Это пример текста, который нужно разбить на слова. Пример текста."
word_frequency = count_word_frequency(test_text)
for word, count in word_frequency.items():
    print(f'{word}: {count}')

import re


with open('data.txt') as file:
    data = file.read()
    birth_date = re.search(r'([\d]{1,2})\.([\d]{1,2})\.([\d]{4})', data)
    # we'll imagine that number is always 10 digits
    phone = re.search(r'(\+[\d]{1,2})?[\d]{10}', data)
    email = re.search(r'[-\w\\.]+@([\w-]+\.)+[\w-]{2,4}', data)

with open('data2.txt', 'w+') as res:
    if birth_date:
        res.write(birth_date.group(0) + '\n')
    if phone:
        res.write(phone.group(0) + '\n')
    if email:
        res.write(email.group(0) + '\n')


import re


def count_unique_words(text):
    words = re.findall(r'\w+', text.lower())
    unique_words = set(words)
    print(f'text contains {len(words)} words, {len(unique_words)} of them are unique')
    i = 1
    for word in unique_words:
        print(f'{i}: {word}')
        i += 1


if __name__ == '__main__':
    x = ('Напишіть функцію, яка буде аналізувати текст, що надходить до неї, і виводити тільки унікальні '
         'слова на екран, загальну кількість слів і кількість унікальних слів')
    count_unique_words(x)

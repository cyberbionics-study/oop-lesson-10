import re


def last_3_symbols():
    text = input('give me something: ')
    splited = re.split(r'[\s]+', text)
    print(splited)
    res = []
    for i in splited:
        if len(i) >= 3:
            res.append(i[-3:])
        else:
            res.append(None)
    return res


if __name__ == '__main__':
    print(last_3_symbols())
